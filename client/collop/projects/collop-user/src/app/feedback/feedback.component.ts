import {Component, OnChanges, OnInit} from '@angular/core';
import {Community} from '../../../../collop-admin/src/app/community';
import {MapVideo} from '../../../../collop-admin/src/app/map-video';
import {fabric} from 'fabric';
import {MapServiceService} from '../../../../collop-admin/src/app/map-service.service';
import {MapVideoService} from '../../../../collop-admin/src/app/map-video.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CommunityService} from '../../../../collop-admin/src/app/community.service';
import {Feedback} from '../feedback';
import {FeedbackService} from '../feedback.service';

@Component({
    selector: 'app-feedback',
    templateUrl: './feedback.component.html',
    styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {

    community: Community = new Community('', 0, 0, []);
    mapImage: string;
    videos: MapVideo[];
    private canvas: fabric.Canvas;
    private activeVideo = new MapVideo('', '', '', 0, 0);
    private previousActiveVideo: MapVideo;
    private feedback: Feedback[];
    private currentlyOpenFeedback: Feedback;
    private storedFeedback: Feedback;

    private isCanvasDragging: boolean;
    private lastPosX: number;
    private lastPosY: number;
    private layerActive = false;

    constructor(private mapService: MapServiceService,
                private mapVideoService: MapVideoService,
                private feedbackService: FeedbackService,
                private route: ActivatedRoute,
                private communityService: CommunityService,
                private router: Router) {
    }

    static calculateImageSizeRatio(image): number {
        return image.get('width') / image.get('height');
    }

    ngOnInit() {
        this.getCommunity();
    }

    openFeedback(feedbackURL: string) {
        this.storeFeedback();
        this.currentlyOpenFeedback = null;

        if (feedbackURL) {
            this.currentlyOpenFeedback = this.feedback.filter(feedback => feedback.url === feedbackURL)[0];
        } else if (this.storedFeedback) {
            this.currentlyOpenFeedback = this.storedFeedback;
            this.storedFeedback = null;
        } else {
            this.currentlyOpenFeedback = new Feedback('', '', '', '');
        }
    }

    storeFeedback() {
        if (this.currentlyOpenFeedback && !this.currentlyOpenFeedback.url &&
            (this.currentlyOpenFeedback.like || this.currentlyOpenFeedback.wish || this.currentlyOpenFeedback.give)) {
            this.storedFeedback = this.currentlyOpenFeedback;
        }
    }

    getCommunity() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.communityService.getCommunity(id).subscribe(
            community => {
                this.community = community;
                this.loadMapImage();
                this.loadMapVideos();
            },
            error => console.log(error)
        );
    }

    loadMapImage() {
        this.mapService.getMap(this.community).subscribe(
            map => {
                this.mapImage = map[0].image;
                this.initializeCanvas();
            },
            error => console.log(error)
        );
    }

    initializeCanvas() {
        this.canvas = new fabric.Canvas('game_area');
        fabric.Image.fromURL(this.mapImage, image => this.canvas.setBackgroundImage(image, this.canvas.renderAll.bind(this.canvas), {
            originX: 'left',
            originY: 'top'
        }));
        this.canvas.selection = false;
        this.setUpEventListeners();
    }

    setUpEventListeners() {
        this.canvas.on('mouse:down', opt => this.activateDragging(opt));
        this.canvas.on('mouse:up', opt => this.stopDragging());
        this.canvas.on('mouse:move', opt => this.dragCanvas(opt));
        this.canvas.on('mouse:wheel', opt => this.zoomCanvas(opt));
    }

    activateDragging(opt) {
        const event = opt.e;
        if (event.altKey === true) {
            this.isCanvasDragging = true;
            this.lastPosX = event.clientX;
            this.lastPosY = event.clientY;
        }
    }

    stopDragging() {
        this.isCanvasDragging = false;
    }

    dragCanvas(opt) {
        if (this.isCanvasDragging) {
            const e = opt.e;
            const newXPosition = e.clientX - this.lastPosX;
            const point = new fabric.Point(newXPosition, e.clientY - this.lastPosY);
            this.canvas.relativePan(point);
            this.keepCanvasInBounds();
            this.canvas.requestRenderAll();
            this.lastPosX = e.clientX;
            this.lastPosY = e.clientY;
        }
    }

    keepCanvasInBounds() {
        if (this.canvas.viewportTransform[4] > 0) {
            this.canvas.viewportTransform[4] = 0;
        }

        const backgroundImage: any = this.canvas.backgroundImage as any;
        if (this.canvas.viewportTransform[4] < this.canvas.getWidth() - backgroundImage.width) {
            this.canvas.viewportTransform[4] = this.canvas.getWidth() - backgroundImage.width;
        }
        if (this.canvas.viewportTransform[5] > 0) {
            this.canvas.viewportTransform[5] = 0;
        }
        if (this.canvas.viewportTransform[5] < this.canvas.getHeight() - backgroundImage.height) {
            this.canvas.viewportTransform[5] = this.canvas.getHeight() - backgroundImage.height;
        }
    }

    zoomCanvas(opt) {
        const delta = -opt.e.deltaY;
        let zoom = this.canvas.getZoom();
        zoom = zoom + delta / 10;
        if (zoom > 10) {
            zoom = 10;
        }
        if (zoom < 1) {
            zoom = 1;
        }
        const point = new fabric.Point(opt.e.offsetX, opt.e.offsetY);
        this.canvas.zoomToPoint(point, zoom);
        opt.e.preventDefault();
        opt.e.stopPropagation();
        const vpt = this.canvas.viewportTransform;
        const backgroundImage: any = this.canvas.backgroundImage as any;
        if (vpt[4] >= 0) {
            this.canvas.viewportTransform[4] = 0;
        } else if (vpt[4] < this.canvas.getWidth() - backgroundImage.width * zoom) {
            this.canvas.viewportTransform[4] = this.canvas.getWidth() - backgroundImage.width * zoom;
        }
        if (vpt[5] >= 0) {
            this.canvas.viewportTransform[5] = 0;
        } else if (vpt[5] < this.canvas.getHeight() - backgroundImage.height * zoom) {
            this.canvas.viewportTransform[5] = this.canvas.getHeight() - backgroundImage.height * zoom;
        }
    }

    loadMapVideos() {
        this.mapVideoService.getMapVideos(this.community).subscribe(
            mapVideos => {
                this.videos = mapVideos;
                this.populateCanvas();
            },
            error => console.log(error)
        );
    }

    scaleImage(image) {
        const ratio = FeedbackComponent.calculateImageSizeRatio(image);
        const maxSize = this.canvas.getWidth() * .1;

        let scale = 1;

        if (ratio > 1) {
            scale = maxSize / image.get('width');
        } else {
            scale = maxSize / image.get('height');
        }

        image.scaleX = scale;
        image.scaleY = scale;
    }

    populateCanvas() {
        this.videos.forEach(video => {
            fabric.Image.fromURL(
                video.thumbnail,
                image => {
                    image.lockRotation = true;
                    image.lockUniScaling = true;
                    image.lockScalingX = true;
                    image.lockScalingY = true;
                    image.set('left', video.position_x);
                    image.set('top', video.position_y);
                    this.scaleImage(image);
                    this.canvas.add(image);

                    this.registerUpdateEventOnImage(image, video);
                });
        });
    }

    registerUpdateEventOnImage(image, video) {
        image.on('mouseup', opt => this.openFeedbackArea(video));
    }

    openFeedbackArea(video) {
        this.openLayerSeparator();
        this.activeVideo = video;
        this.getFeedback();
    }

    getFeedback() {
        if (this.activeVideo.url && (!this.previousActiveVideo || this.previousActiveVideo.url !== this.activeVideo.url)) {
            this.previousActiveVideo = this.activeVideo;
            this.feedbackService.getFeedback(this.activeVideo).subscribe(
                feedback => {
                    this.feedback = feedback;
                    for (let i = feedback.length; i < 12; i++) {
                        this.feedback.push(new Feedback('', '', '', ''));
                    }
                },
                error => console.log(error)
            );
        }
    }

    sendFeedback() {
        if (!this.currentlyOpenFeedback.url) {
            this.currentlyOpenFeedback.map_video = this.activeVideo.url;
            this.feedbackService.saveFeedback(this.currentlyOpenFeedback).subscribe(
                feedback => {
                    this.updateFeedbackButtons(feedback);
                    this.currentlyOpenFeedback = feedback;
                    this.reload();
                },
                error => console.log(error)
            );
        }
    }

    reload() {
        window.location.reload();
    }

    updateFeedbackButtons(feedback) {
        for (let i = 0; i < this.feedback.length; i++) {
            if (!this.feedback[i].url) {
                this.feedback[i] = feedback;
                break;
            }
        }
    }

    onLikeChanges(event) {
        this.currentlyOpenFeedback.like = this.getValueFromEvent(event);
    }

    onWishChanges(event) {
        this.currentlyOpenFeedback.wish = this.getValueFromEvent(event);
    }

    onGiveChanges(event) {
        this.currentlyOpenFeedback.give = this.getValueFromEvent(event);
    }

    getValueFromEvent(event): string {
        return event.target.value;
    }

    openLayerSeparator() {
        this.layerActive = true;
    }

    closeLayerSeparator() {
        this.layerActive = false;
    }
}
