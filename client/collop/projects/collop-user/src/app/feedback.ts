export class Feedback {
    constructor(
        public map_video: string,
        public like: string,
        public wish: string,
        public give: string,
        public url?: string
    ) {
    }
}
