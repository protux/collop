import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {OpeningComponent} from './opening/opening.component';
import {UserDashboardComponent} from './user-dashboard/user-dashboard.component';
import {LearnVectorsComponent} from './learn-vectors/learn-vectors.component';
import {QuoteComponent} from './quote/quote.component';
import {FeedbackComponent} from './feedback/feedback.component';
import {ViewVideosComponent} from './view-videos/view-videos.component';


const routes: Routes = [
    {path: '', component: UserDashboardComponent},
    {path: 'community/:id', component: OpeningComponent},
    {path: 'community/:id/quote', component: QuoteComponent},
    {path: 'community/:id/vectors', component: LearnVectorsComponent},
    {path: 'community/:id/view', component: ViewVideosComponent},
    {path: 'community/:id/feedback', component: FeedbackComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
