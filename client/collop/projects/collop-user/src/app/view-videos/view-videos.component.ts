import {Component, OnInit} from '@angular/core';
import {MapServiceService} from '../../../../collop-admin/src/app/map-service.service';
import {MapVideoService} from '../../../../collop-admin/src/app/map-video.service';
import {Community} from '../../../../collop-admin/src/app/community';
import {MapVideo} from '../../../../collop-admin/src/app/map-video';
import {fabric} from 'fabric';
import {ActivatedRoute} from '@angular/router';
import {CommunityService} from '../../../../collop-admin/src/app/community.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-view-videos',
    templateUrl: './view-videos.component.html',
    styleUrls: ['./view-videos.component.scss']
})
export class ViewVideosComponent implements OnInit {

    community: Community = new Community('', 0, 0, []);
    mapImage: string;
    videos: MapVideo[];
    private canvas: fabric.Canvas;
    private activeVideoId = '';

    private isCanvasDragging: boolean;
    private lastPosX: number;
    private lastPosY: number;
    private layerActive = false;

    constructor(private mapService: MapServiceService,
                private mapVideoService: MapVideoService,
                private route: ActivatedRoute,
                private communityService: CommunityService,
                private modalService: NgbModal) {
    }

    static calculateImageSizeRatio(image): number {
        return image.get('width') / image.get('height');
    }

    ngOnInit() {
        this.getCommunity();
    }

    getCommunity() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.communityService.getCommunity(id).subscribe(
            community => {
                this.community = community;
                this.loadMapImage();
                this.loadMapVideos();
            },
            error => console.log(error)
        );
    }

    loadMapImage() {
        this.mapService.getMap(this.community).subscribe(
            map => {
                this.mapImage = map[0].image;
                this.initializeCanvas();
            },
            error => console.log(error)
        );
    }

    initializeCanvas() {
        this.canvas = new fabric.Canvas('game_area');
        fabric.Image.fromURL(this.mapImage, image => this.canvas.setBackgroundImage(image, this.canvas.renderAll.bind(this.canvas), {
            originX: 'left',
            originY: 'top'
        }));
        this.canvas.selection = false;
        this.setUpEventListeners();
    }

    setUpEventListeners() {
        this.canvas.on('mouse:down', opt => this.activateDragging(opt));
        this.canvas.on('mouse:up', opt => this.stopDragging());
        this.canvas.on('mouse:move', opt => this.dragCanvas(opt));
        this.canvas.on('mouse:wheel', opt => this.zoomCanvas(opt));
    }

    activateDragging(opt) {
        const event = opt.e;
        if (event.altKey === true) {
            this.isCanvasDragging = true;
            this.lastPosX = event.clientX;
            this.lastPosY = event.clientY;
        }
    }

    stopDragging() {
        this.isCanvasDragging = false;
    }

    dragCanvas(opt) {
        if (this.isCanvasDragging) {
            const e = opt.e;
            const newXPosition = e.clientX - this.lastPosX;
            const point = new fabric.Point(newXPosition, e.clientY - this.lastPosY);
            this.canvas.relativePan(point);
            this.keepCanvasInBounds();
            this.canvas.requestRenderAll();
            this.lastPosX = e.clientX;
            this.lastPosY = e.clientY;
        }
    }

    keepCanvasInBounds() {
        if (this.canvas.viewportTransform[4] > 0) {
            this.canvas.viewportTransform[4] = 0;
        }

        const backgroundImage: any = this.canvas.backgroundImage as any;
        if (this.canvas.viewportTransform[4] < this.canvas.getWidth() - backgroundImage.width) {
            this.canvas.viewportTransform[4] = this.canvas.getWidth() - backgroundImage.width;
        }
        if (this.canvas.viewportTransform[5] > 0) {
            this.canvas.viewportTransform[5] = 0;
        }
        if (this.canvas.viewportTransform[5] < this.canvas.getHeight() - backgroundImage.height) {
            this.canvas.viewportTransform[5] = this.canvas.getHeight() - backgroundImage.height;
        }
    }

    zoomCanvas(opt) {
        const delta = -opt.e.deltaY;
        let zoom = this.canvas.getZoom();
        zoom = zoom + delta / 10;
        if (zoom > 10) {
            zoom = 10;
        }
        if (zoom < 1) {
            zoom = 1;
        }
        const point = new fabric.Point(opt.e.offsetX, opt.e.offsetY);
        this.canvas.zoomToPoint(point, zoom);
        opt.e.preventDefault();
        opt.e.stopPropagation();
        const vpt = this.canvas.viewportTransform;
        const backgroundImage: any = this.canvas.backgroundImage as any;
        if (vpt[4] >= 0) {
            this.canvas.viewportTransform[4] = 0;
        } else if (vpt[4] < this.canvas.getWidth() - backgroundImage.width * zoom) {
            this.canvas.viewportTransform[4] = this.canvas.getWidth() - backgroundImage.width * zoom;
        }
        if (vpt[5] >= 0) {
            this.canvas.viewportTransform[5] = 0;
        } else if (vpt[5] < this.canvas.getHeight() - backgroundImage.height * zoom) {
            this.canvas.viewportTransform[5] = this.canvas.getHeight() - backgroundImage.height * zoom;
        }
    }

    loadMapVideos() {
        this.mapVideoService.getMapVideos(this.community).subscribe(
            mapVideos => {
                this.videos = mapVideos;
                this.populateCanvas();
            },
            error => console.log(error)
        );
    }

    scaleImage(image) {
        const ratio = ViewVideosComponent.calculateImageSizeRatio(image);
        const maxSize = this.canvas.getWidth() * .1;

        let scale = 1;

        if (ratio > 1) {
            scale = maxSize / image.get('width');
        } else {
            scale = maxSize / image.get('height');
        }

        image.scaleX = scale;
        image.scaleY = scale;
    }

    populateCanvas() {
        this.videos.forEach(video => {
            fabric.Image.fromURL(
                video.thumbnail,
                image => {
                    const ratio = ViewVideosComponent.calculateImageSizeRatio(image);
                    image.lockRotation = true;
                    image.lockUniScaling = true;
                    image.lockScalingX = true;
                    image.lockScalingY = true;
                    image.set('left', video.position_x);
                    image.set('top', video.position_y);
                    this.scaleImage(image);
                    this.canvas.add(image);

                    this.registerUpdateEventOnImage(image, video);
                });
        });
    }

    registerUpdateEventOnImage(image, video) {
        image.on('mouseup', opt => this.openVideoToWatch(image, video));
    }

    openVideoToWatch(image, video) {
        this.openLayerSeparator();
        this.activeVideoId = video.video_id;
    }

    openLayerSeparator() {
        this.layerActive = true;
    }

    closeLayerSeparator() {
        this.layerActive = false;
    }
}
