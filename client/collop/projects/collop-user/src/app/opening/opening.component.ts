import {Component, OnChanges, OnInit} from '@angular/core';
import {CommunityService} from '../../../../collop-admin/src/app/community.service';
import {Community} from '../../../../collop-admin/src/app/community';
import {ActivatedRoute} from '@angular/router';
import {OpeningVideo} from '../../../../collop-admin/src/app/opening-video';
import {OpeningVideoService} from '../../../../collop-admin/src/app/opening-video.service';

@Component({
    selector: 'app-opening',
    templateUrl: './opening.component.html',
    styleUrls: ['./opening.component.scss']
})
export class OpeningComponent implements OnInit {

    community: Community = new Community('', 0, 0, []);
    openingVideo: OpeningVideo = new OpeningVideo('', '', '');

    constructor(
        private route: ActivatedRoute,
        private communityService: CommunityService,
        private openingVideoService: OpeningVideoService) {
    }

    ngOnInit() {
        this.getCommunity();
    }

    getCommunity(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.communityService.getCommunity(id).subscribe(
            community => {
                this.community = community;
                this.loadOpeningVideo();
            },
            error => console.log(error)
        );
    }

    loadOpeningVideo() {
        this.openingVideoService.getOpeningVideos(this.community).subscribe(
            openingVideos => this.openingVideo = openingVideos[0],
            error => console.log(error)
        );
    }
}
