import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LearnVectorsComponent} from './learn-vectors.component';

describe('LearnVectorsComponent', () => {
    let component: LearnVectorsComponent;
    let fixture: ComponentFixture<LearnVectorsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LearnVectorsComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LearnVectorsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
