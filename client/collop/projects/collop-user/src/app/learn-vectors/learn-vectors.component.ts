import {Component, Input, OnInit} from '@angular/core';
import {Community} from '../../../../collop-admin/src/app/community';
import {CommunityService} from '../../../../collop-admin/src/app/community.service';
import {ActivatedRoute} from '@angular/router';
import {Vector} from '../../../../collop-admin/src/app/vector';

@Component({
    selector: 'app-learn-vectors',
    templateUrl: './learn-vectors.component.html',
    styleUrls: ['./learn-vectors.component.scss']
})
export class LearnVectorsComponent implements OnInit {

    @Input()
    community: Community = new Community('', 0, 0, []);
    staticVectors: Vector[] = [];
    dynamicVectors: Vector[] = [];

    constructor(private route: ActivatedRoute, private communityService: CommunityService) {
    }

    ngOnInit() {
        this.getCommunity();
    }

    getCommunity() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.communityService.getCommunity(id).subscribe(
            community => {
                this.community = community;
                this.staticVectors = this.community.vectors.filter(vector => vector.vector_type === 'static');
                this.dynamicVectors = this.community.vectors.filter(vector => vector.vector_type === 'dynamic');
            },
            error => console.log(error)
        );
    }
}
