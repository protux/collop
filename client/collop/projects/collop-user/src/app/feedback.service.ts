import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {MapVideo} from '../../../collop-admin/src/app/map-video';
import {Feedback} from './feedback';
import {catchError} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class FeedbackService {

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    feedbacksURL = 'http://localhost:8000/api/feedbacks';

    constructor(private http: HttpClient) {
    }

    getFeedback(mapVideo: MapVideo): Observable<Feedback[]> {
        const urlParts = mapVideo.url.split('/');
        const url = `${this.feedbacksURL}?map_video=${urlParts[urlParts.length - 1]}`;
        return this.http.get<Feedback[]>(url).pipe(
            catchError(this.handleError<Feedback[]>('getFeedback'))
        );
    }

    saveFeedback(feedback: Feedback): Observable<Feedback> {
        return this.http.post<Feedback>(this.feedbacksURL, feedback, this.httpOptions).pipe(
            catchError(this.handleError<Feedback>('saveFeedback'))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result);
        };
    }
}
