import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {OpeningComponent} from './opening/opening.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {UserDashboardComponent} from './user-dashboard/user-dashboard.component';
import {CommunityChooserComponent} from '../../../collop-admin/src/app/community-chooser/community-chooser.component';
import {YoutubeEmbeddedComponent} from '../../../collop-admin/src/app/youtube-embedded/youtube-embedded.component';
import { LearnVectorsComponent } from './learn-vectors/learn-vectors.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { QuoteComponent } from './quote/quote.component';
import { ViewVideosComponent } from './view-videos/view-videos.component';

@NgModule({
    declarations: [
        AppComponent,
        OpeningComponent,
        UserDashboardComponent,
        CommunityChooserComponent,
        YoutubeEmbeddedComponent,
        LearnVectorsComponent,
        FeedbackComponent,
        QuoteComponent,
        ViewVideosComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
    entryComponents: [UserDashboardComponent]
})
export class AppModule {
}
