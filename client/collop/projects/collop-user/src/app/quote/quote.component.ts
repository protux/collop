import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-quote',
    templateUrl: './quote.component.html',
    styleUrls: ['./quote.component.scss']
})
export class QuoteComponent implements OnInit {

    communityId = 0;

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.communityId = +this.route.snapshot.paramMap.get('id');
    }
}
