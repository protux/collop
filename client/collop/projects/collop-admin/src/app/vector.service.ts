import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Vector} from './vector';
import {catchError} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class VectorService {

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    vectorsURL = 'http://localhost:8000/api/vectors';

    constructor(private http: HttpClient) {
    }

    getVector(id): Observable<Vector> {
        return this.http.get<Vector>(`${this.vectorsURL}/${id}`).pipe(
            catchError(this.handleError<Vector>('getVector(id)'))
        );
    }

    getVectors(): Observable<Vector[]> {
        return this.http.get<Vector[]>(this.vectorsURL).pipe(
            catchError(this.handleError<Vector[]>('getVectors'))
        );
    }

    saveVector(vector): Observable<Vector> {
        if (!vector.url) {
            return this.addVector(vector);
        } else {
            return this.updateVector(vector);
        }
    }

    private addVector(vector): Observable<Vector> {
        return this.http.post<Vector>(this.vectorsURL, vector, this.httpOptions).pipe(
            catchError(this.handleError<Vector>('addVector'))
        );
    }

    private updateVector(vector): Observable<Vector> {
        return this.http.put<Vector>(vector.url, vector, this.httpOptions).pipe(
            catchError(this.handleError<Vector>('addVector'))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result);
        };
    }
}
