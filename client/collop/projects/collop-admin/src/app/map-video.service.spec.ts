import {TestBed} from '@angular/core/testing';

import {MapVideoService} from './map-video.service';

describe('MapVideoService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: MapVideoService = TestBed.get(MapVideoService);
        expect(service).toBeTruthy();
    });
});
