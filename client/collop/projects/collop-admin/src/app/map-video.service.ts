import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MapVideo} from './map-video';
import {Observable, of} from 'rxjs';
import {Community} from './community';
import {catchError} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class MapVideoService {

    mapVideosURL = 'http://localhost:8000/api/map-videos';

    constructor(private http: HttpClient) {
    }

    getMapVideos(community: Community): Observable<MapVideo[]> {
        const mapUrl = `${this.mapVideosURL}?community=${community.id}`;
        return this.http.get<MapVideo[]>(mapUrl).pipe(
            catchError(this.handleError<MapVideo[]>('getMapVideos'))
        );
    }

    updateMapVideoPosition(video: MapVideo): Observable<MapVideo> {
        console.log(video);
        return this.http.patch<MapVideo>(video.url, {position_x: video.position_x, position_y: video.position_y}).pipe(
            catchError(this.handleError<MapVideo>('updateMap'))
        );
    }

    createMapVideo(community: Community, formData: FormData): Observable<MapVideo> {
        formData.append('community', community.url);
        return this.http.post<MapVideo>(this.mapVideosURL, formData).pipe(
            catchError(this.handleError<MapVideo>('createMapVideo'))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result);
        };
    }
}
