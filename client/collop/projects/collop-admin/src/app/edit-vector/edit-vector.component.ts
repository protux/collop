import {Component, Input, OnInit} from '@angular/core';
import {Vector} from '../vector';
import {VectorService} from '../vector.service';
import {Location} from '@angular/common';
import {NumberValueAccessor} from '@angular/forms';

@Component({
    selector: 'app-edit-vector',
    templateUrl: './edit-vector.component.html',
    styleUrls: ['./edit-vector.component.scss']
})
export class EditVectorComponent implements OnInit {

    @Input()
    vector = new Vector('', '', '');
    submitted = false;

    constructor(private vectorService: VectorService, private location: Location) {
    }

    onSubmit(): void {
        this.submitted = true;
        this.vectorService.saveVector(this.vector).subscribe(
            vector => {
                this.vector = vector;
                this.submitted = false;
            },
            error => this.submissionFailed()
        );
    }

    ngOnInit() {
        if (!this.vector.name || this.vector.name.length === 0) {
            const splittedPath = location.pathname.split('/');
            const lastSegment = splittedPath[splittedPath.length - 1];

            if (lastSegment.match('\\d+')) {
                const id = Number.parseInt(lastSegment);
                this.vectorService.getVector(id).subscribe(
                    vector => this.vector = vector,
                    error => console.log(error)
                );
            }
        }
    }

    submissionFailed(): void {
        this.submitted = false;
    }

    onCancel(): void {
        this.location.back();
    }
}
