import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Community} from './community';
import {catchError} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {OpeningVideo} from './opening-video';

@Injectable({
    providedIn: 'root'
})
export class OpeningVideoService {

    openingVideosURL = 'http://localhost:8000/api/opening-videos';

    constructor(private http: HttpClient) {
    }

    getOpeningVideos(community: Community): Observable<OpeningVideo[]> {
        const openingVideoUrl = `${this.openingVideosURL}?community=${community.id}`;
        return this.http.get<OpeningVideo[]>(openingVideoUrl).pipe(
            catchError(this.handleError<OpeningVideo[]>('getOpeningVideos'))
        );
    }

    createOpeningVideo(community: Community, formData: FormData): Observable<OpeningVideo> {
        formData.append('community', community.url);
        return this.http.post<OpeningVideo>(this.openingVideosURL, formData).pipe(
            catchError(this.handleError<OpeningVideo>('createOpeningVideo'))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result);
        };
    }
}
