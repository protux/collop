import {TestBed} from '@angular/core/testing';

import {OpeningVideoService} from './opening-video.service';

describe('OpeningVideoService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: OpeningVideoService = TestBed.get(OpeningVideoService);
        expect(service).toBeTruthy();
    });
});
