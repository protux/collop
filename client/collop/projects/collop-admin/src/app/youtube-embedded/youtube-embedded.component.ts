import {Component, Input, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-youtube-embedded',
    templateUrl: './youtube-embedded.component.html',
    styleUrls: ['./youtube-embedded.component.scss']
})
export class YoutubeEmbeddedComponent implements OnInit {

    @Input()
    youtubeId = 'PLACEHOLDER';

    constructor(private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
    }

    youTubeURL() {
        const url = `https://www.youtube-nocookie.com/embed/${this.youtubeId}`;
        console.log(url);
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}
