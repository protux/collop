import {Component, OnInit} from '@angular/core';
import {Vector} from '../vector';
import {VectorService} from '../vector.service';

@Component({
    selector: 'app-vectors-list',
    templateUrl: './vectors-list.component.html',
    styleUrls: ['./vectors-list.component.scss']
})
export class VectorsListComponent implements OnInit {

    staticVectors: Vector[] = [];
    dynamicVectors: Vector[] = [];

    constructor(private vectorService: VectorService) {
    }

    getVectors(): void {
        this.vectorService.getVectors().subscribe(
            vectors => {
                vectors.forEach(v => console.log(v.vector_type));
                this.staticVectors = vectors.filter(vector => vector.vector_type === 'static');
                this.dynamicVectors = vectors.filter(vector => vector.vector_type === 'dynamic');
                console.log(this.dynamicVectors.length);
            },
            error => console.log(error)
        );
    }

    ngOnInit() {
        this.getVectors();
    }
}
