import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {VectorsListComponent} from './vectors-list.component';

describe('VectorsListComponent', () => {
    let component: VectorsListComponent;
    let fixture: ComponentFixture<VectorsListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [VectorsListComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VectorsListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
