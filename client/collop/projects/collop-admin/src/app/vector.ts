export class Vector {
    constructor(
        public name: string,
        public video_id: string,
        public vector_type: string,
        public url?: string,
        public id?
    ) {
    }
}
