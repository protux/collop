import {Component, Input, OnInit} from '@angular/core';
import {Community} from '../community';
import {CommunityService} from '../community.service';
import {Location} from '@angular/common';
import {Router} from '@angular/router';

@Component({
    selector: 'app-create-project',
    templateUrl: './edit-community.component.html',
    styleUrls: ['./edit-community.component.scss']
})
export class EditCommunityComponent implements OnInit {

    @Input()
    community: Community = new Community('', 0.0, 0.0, []);

    submitted = false;

    constructor(private communityService: CommunityService, private location: Location, private router: Router) {
    }

    onSubmit(): void {
        this.submitted = true;
        this.communityService.saveCommunity(this.community).subscribe(
            community => this.router.navigate(['community/', community.id]).finally(() => {
                this.community = community;
                this.submitted = false;
            }),
            error => {
                console.log(error);
                this.submissionFailed();
            }
        );
    }

    submissionFailed(): void {
        this.submitted = false;
    }

    onCancel(): void {
        this.location.back();
    }

    ngOnInit() {
    }
}
