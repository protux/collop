export class OpeningVideo {
    constructor(
        public community: string,
        public video_id: string,
        public thumbnail: string,
        public url?: string
    ) {
    }
}
