export class MapVideo {
    constructor(
        public community: string,
        public video_id: string,
        public thumbnail: string,
        public position_x: number,
        public position_y: number,
        public url?: string
    ) {
    }
}
