import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EditCommunityBackgroundComponent} from './edit-community-background.component';

describe('EditCommunityBackgroundComponent', () => {
    let component: EditCommunityBackgroundComponent;
    let fixture: ComponentFixture<EditCommunityBackgroundComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EditCommunityBackgroundComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EditCommunityBackgroundComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
