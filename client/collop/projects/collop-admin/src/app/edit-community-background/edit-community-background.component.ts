import {AfterViewInit, Component, Input, OnChanges, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MapServiceService} from '../map-service.service';
import {Community} from '../community';

@Component({
    selector: 'app-edit-community-background',
    templateUrl: './edit-community-background.component.html',
    styleUrls: ['./edit-community-background.component.scss']
})
export class EditCommunityBackgroundComponent implements OnInit, OnChanges {
    @Input()
    private community: Community = new Community('', 0, 0, []);
    private form: FormGroup;
    private imageURL;

    constructor(private formBuilder: FormBuilder, private mapService: MapServiceService) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            background: ['']
        });
    }

    ngOnChanges() {
        if (!this.imageURL && this.community.id) {
            this.mapService.getMap(this.community).subscribe(
                map => this.imageURL = map[0].image,
                error => console.log(error)
            );
        }
    }

    onSubmit() {
        const formData = new FormData();
        formData.append('image', this.form.get('background').value);

        this.mapService.createMap(this.community, formData).subscribe(
            response => this.imageURL = `${response.image}`,
            err => console.log(err)
        );
    }

    onChange(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.form.get('background').setValue(file);
        }
    }
}
