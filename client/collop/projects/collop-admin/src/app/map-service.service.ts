import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Community} from './community';
import {catchError} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class MapServiceService {

    mapsURL = 'http://localhost:8000/api/maps';

    constructor(private http: HttpClient) {
    }

    getMap(community: Community): Observable<object> {
        const mapUrl = `${this.mapsURL}?community=${community.id}`;
        return this.http.get<object>(mapUrl).pipe(
            catchError(this.handleError<object>('getMap'))
        );
    }

    createMap(community: Community, formData: FormData): Observable<any> {
        formData.append('community', community.url);
        return this.http.post<any>(this.mapsURL, formData);
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result);
        };
    }
}
