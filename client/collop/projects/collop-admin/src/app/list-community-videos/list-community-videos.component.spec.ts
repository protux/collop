import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ListCommunityVideosComponent} from './list-community-videos.component';

describe('ListCommunityVideosComponent', () => {
    let component: ListCommunityVideosComponent;
    let fixture: ComponentFixture<ListCommunityVideosComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ListCommunityVideosComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ListCommunityVideosComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
