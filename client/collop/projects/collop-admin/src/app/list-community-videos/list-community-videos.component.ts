import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Community} from '../community';
import {MapVideoService} from '../map-video.service';
import {MapVideo} from '../map-video';

@Component({
    selector: 'app-list-community-videos',
    templateUrl: './list-community-videos.component.html',
    styleUrls: ['./list-community-videos.component.scss']
})
export class ListCommunityVideosComponent implements OnInit, OnChanges {

    @Input()
    community: Community = new Community('', 0, 0, []);
    mapVideos: MapVideo[] = [];

    constructor(private mapVideoService: MapVideoService) {
    }

    ngOnInit() {
    }

    ngOnChanges() {
        if (this.community.id) {
            this.mapVideoService.getMapVideos(this.community).subscribe(
                mapVideos => this.mapVideos = mapVideos,
                error => console.log(error)
            );
        }
    }
}
