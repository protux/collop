import {Component, OnInit} from '@angular/core';
import {Community} from '../community';
import {CommunityService} from '../community.service';

@Component({
    selector: 'app-community-chooser',
    templateUrl: './community-chooser.component.html',
    styleUrls: ['./community-chooser.component.scss']
})
export class CommunityChooserComponent implements OnInit {

    communities: Community[] = [];

    constructor(private communityService: CommunityService) {
    }

    ngOnInit() {
        this.communityService.getCommunities().subscribe(
            communities => this.communities = communities,
            error => console.log('Error fetching communities')
        );
    }
}
