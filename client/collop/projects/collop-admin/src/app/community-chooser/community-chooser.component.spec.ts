import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CommunityChooserComponent} from './community-chooser.component';

describe('CommunityChooserComponent', () => {
    let component: CommunityChooserComponent;
    let fixture: ComponentFixture<CommunityChooserComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CommunityChooserComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CommunityChooserComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
