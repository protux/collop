import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CommonModule} from '@angular/common';
import {EditCommunityComponent} from './edit-community/edit-community.component';
import {CommunityDashboardComponent} from './community-dashboard/community-dashboard.component';
import {CommunityChooserComponent} from './community-chooser/community-chooser.component';
import {VectorsListComponent} from './vectors-list/vectors-list.component';
import {EditVectorComponent} from './edit-vector/edit-vector.component';


const routes: Routes = [
    {path: '', component: DashboardComponent},
    {path: 'create-community', component: EditCommunityComponent},
    {path: 'community/:id', component: CommunityDashboardComponent},
    {path: 'communities', component: CommunityChooserComponent},
    {path: 'vectors', component: VectorsListComponent},
    {path: 'vectors/:id', component: EditVectorComponent},
    {path: 'create-vector', component: EditVectorComponent}
];

@NgModule({
    imports: [CommonModule, RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
