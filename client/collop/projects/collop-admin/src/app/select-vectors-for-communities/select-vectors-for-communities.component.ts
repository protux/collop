import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Community} from '../community';
import {Vector} from '../vector';
import {VectorService} from '../vector.service';
import {CommunityService} from '../community.service';

@Component({
    selector: 'app-select-vectors-for-communities',
    templateUrl: './select-vectors-for-communities.component.html',
    styleUrls: ['./select-vectors-for-communities.component.scss']
})
export class SelectVectorsForCommunitiesComponent implements OnInit, OnChanges {

    @Input()
    community = new Community('', 0, 0, []);

    availableVectors: Vector[] = [];

    persistentUnselectedVectors: Vector[] = [];
    persistentSelectedVectors: Vector[] = [];

    selectSelected = false;
    unselectSelected = false;
    selectedToAdd: string;
    selectedToRemove: string;

    private empty = '----------';

    constructor(private vectorService: VectorService, private communityService: CommunityService) {
    }

    ngOnInit() {
    }

    ngOnChanges() {
        if (this.community.id) {
            this.persistentSelectedVectors = this.community.vectors;
            this.loadAvailableVectors();
        }
    }

    private loadAvailableVectors() {
        this.vectorService.getVectors().subscribe(
            vectors => {
                this.availableVectors = vectors;
                this.persistentUnselectedVectors = vectors.filter(x => this.persistentSelectedVectors.findIndex(v => v.id === x.id) < 0);
            },
            error => console.log(error)
        );
    }

    private selectDeactivated(): boolean {
        return !this.selectSelected;
    }

    private unselectDeactivate(): boolean {
        return !this.unselectSelected;
    }

    private activateSelect(selectedValue) {
        if (selectedValue) {
            this.selectedToAdd = selectedValue;
            this.selectSelected = true;
        }
    }

    private activateUnselect(selectedValue) {
        if (selectedValue) {
            this.selectedToRemove = selectedValue;
            this.unselectSelected = true;
        }
    }

    private removeSelected(): void {
        const index = this.persistentSelectedVectors.findIndex(vector => vector.url === this.selectedToRemove);
        if (index >= 0) {
            const removedVector = this.persistentSelectedVectors.splice(index, 1)[0];
            this.persistentUnselectedVectors.push(removedVector);
            this.communityService.removeVectorFromCommunity(this.community, removedVector).subscribe(
                _ => console.log('removed vector'),
                error => console.log(error)
            );
            this.unselectSelected = false;
        }
    }

    private addSelected(): void {
        const index = this.persistentUnselectedVectors.findIndex(vector => vector.url === this.selectedToAdd);
        if (index >= 0) {
            const addedVector = this.persistentUnselectedVectors.splice(index, 1)[0];
            this.persistentSelectedVectors.push(addedVector);
            this.communityService.addVectorToCommunity(this.community, addedVector).subscribe(
                _ => console.log('added vector to community'),
                error => console.log(error)
            );
            this.selectSelected = false;
        }
    }
}
