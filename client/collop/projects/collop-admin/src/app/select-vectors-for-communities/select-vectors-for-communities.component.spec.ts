import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SelectVectorsForCommunitiesComponent} from './select-vectors-for-communities.component';

describe('SelectVectorsForCommunitiesComponent', () => {
    let component: SelectVectorsForCommunitiesComponent;
    let fixture: ComponentFixture<SelectVectorsForCommunitiesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SelectVectorsForCommunitiesComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SelectVectorsForCommunitiesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
