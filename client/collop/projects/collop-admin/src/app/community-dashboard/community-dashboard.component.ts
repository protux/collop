import {Component, Input, OnInit} from '@angular/core';
import {Community} from '../community';
import {ActivatedRoute} from '@angular/router';
import {CommunityService} from '../community.service';

@Component({
    selector: 'app-community-dashboard',
    templateUrl: './community-dashboard.component.html',
    styleUrls: ['./community-dashboard.component.scss']
})
export class CommunityDashboardComponent implements OnInit {

    @Input()
    community: Community = new Community('', 0, 0, []);

    constructor(private route: ActivatedRoute, private communityService: CommunityService) {
    }

    ngOnInit() {
      this.getCommunity();
    }

    getCommunity(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.communityService.getCommunity(id).subscribe(
            community => this.community = community,
            error => console.log(error)
        );
    }
}
