import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Community} from '../community';
import {FormBuilder, FormGroup} from '@angular/forms';
import {OpeningVideoService} from '../opening-video.service';
import {OpeningVideo} from '../opening-video';

@Component({
    selector: 'app-add-opening-video',
    templateUrl: './add-opening-video.component.html',
    styleUrls: ['./add-opening-video.component.scss']
})
export class AddOpeningVideoComponent implements OnInit, OnChanges {

    @Input()
    private community: Community = new Community('', 0, 0, []);
    private form: FormGroup;
    private openingVideo: OpeningVideo;

    constructor(private formBuilder: FormBuilder, private openingVideoService: OpeningVideoService) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            thumbnail: [''],
            video_id: ''
        });
    }

    ngOnChanges() {
        if (this.community.id) {
            this.openingVideoService.getOpeningVideos(this.community).subscribe(
                openingVideo => {
                    console.log(openingVideo[0].video_id);
                    this.openingVideo = openingVideo[0];
                },
                error => console.log(error)
            );
        }
    }

    onSubmit() {
        const formData = new FormData();
        formData.append('thumbnail', this.form.get('thumbnail').value);
        formData.append('video_id', this.form.get('video_id').value);

        this.openingVideoService.createOpeningVideo(this.community, formData).subscribe(
            openingVideo => this.openingVideo = openingVideo,
            error => console.log(error)
        );
    }

    onThumbnailChange(event) {
        if (event.target.files.length === 1) {
            const file = event.target.files[0];
            this.form.get('thumbnail').setValue(file);
        }
    }

    onVideoIdChange(event) {
        const videoId = event.target.value;
        this.form.get('video_id').setValue(videoId);
    }
}
