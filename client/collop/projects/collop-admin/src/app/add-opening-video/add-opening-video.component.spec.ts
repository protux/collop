import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddOpeningVideoComponent} from './add-opening-video.component';

describe('AddOpeningVideoComponent', () => {
    let component: AddOpeningVideoComponent;
    let fixture: ComponentFixture<AddOpeningVideoComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AddOpeningVideoComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddOpeningVideoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
