import {Injectable} from '@angular/core';
import {Community} from './community';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Vector} from './vector';

@Injectable({
    providedIn: 'root'
})
export class CommunityService {

    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    communitiesURL = 'http://localhost:8000/api/communities';

    constructor(private http: HttpClient) {
    }

    getCommunities(): Observable<Community[]> {
        return this.http.get<Community[]>(this.communitiesURL).pipe(
            catchError(this.handleError<Community[]>('getCommunities'))
        );
    }

    saveCommunity(community: Community): Observable<Community> {
        if (!community.id) {
            return this.addCommunity(community);
        } else {
            return this.updateCommunity(community);
        }
    }

    addCommunity(community: Community): Observable<Community> {
        console.log('adding community');
        return this.http.post<Community>(this.communitiesURL, community, this.httpOptions).pipe(
            catchError(this.handleError<Community>('addCommunity'))
        );
    }

    updateCommunity(community: Community): Observable<Community> {
        console.log('updating community');
        return this.http.put<Community>(`${this.communitiesURL}/${community.id}`, community, this.httpOptions).pipe(
            catchError(this.handleError<Community>('addCommunity'))
        );
    }

    getCommunity(id: number): Observable<Community> {
        const url = `${this.communitiesURL}/${id}`;
        return this.http.get<Community>(url).pipe(
            catchError(this.handleError<Community>('getCommunity'))
        );
    }

    addVectorToCommunity(community: Community, vector: Vector) {
        const url = `${this.communitiesURL}/${community.id}/add-vector?vector=${vector.id}`;
        return this.http.post(url, null, this.httpOptions).pipe(
            catchError(this.handleError('addCommunityVector'))
        );
    }

    removeVectorFromCommunity(community: Community, vector: Vector): Observable<object> {
        const url = `${this.communitiesURL}/${community.id}/remove-vector?vector=${vector.id}`;
        return this.http.delete(url).pipe(
            catchError(this.handleError('removeCommunityVector'))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result);
        };
    }
}
