import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AddMapVideosComponent} from './add-map-videos.component';

describe('AddMapVideosComponent', () => {
    let component: AddMapVideosComponent;
    let fixture: ComponentFixture<AddMapVideosComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AddMapVideosComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddMapVideosComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
