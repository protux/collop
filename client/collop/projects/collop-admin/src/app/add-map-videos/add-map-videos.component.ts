import {Component, Input, OnInit} from '@angular/core';
import {Community} from '../community';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MapVideoService} from '../map-video.service';
import {MapVideo} from '../map-video';

@Component({
    selector: 'app-add-map-videos',
    templateUrl: './add-map-videos.component.html',
    styleUrls: ['./add-map-videos.component.scss']
})
export class AddMapVideosComponent implements OnInit {
    @Input()
    private community: Community = new Community('', 0, 0, []);
    private form: FormGroup;
    private mapVideos: MapVideo[] = [];

    constructor(private formBuilder: FormBuilder, private mapVideoService: MapVideoService) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            thumbnail: [''],
            video_id: ''
        });
    }

    onSubmit() {
        const formData = new FormData();
        formData.append('thumbnail', this.form.get('thumbnail').value);
        formData.append('video_id', this.form.get('video_id').value);

        this.mapVideoService.createMapVideo(this.community, formData).subscribe(
            mapVideo => this.mapVideos.push(mapVideo),
            err => console.log(err)
        );
    }

    onThumbnailChange(event) {
        if (event.target.files.length > 0) {
            const file = event.target.files[0];
            this.form.get('thumbnail').setValue(file);
        }
    }

    onVideoIdChange(event) {
        const videoId = event.target.value;
        this.form.get('video_id').setValue(videoId);
    }
}
