import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {EditCommunityComponent} from './edit-community/edit-community.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { CommunityDashboardComponent } from './community-dashboard/community-dashboard.component';
import { CommunityChooserComponent } from './community-chooser/community-chooser.component';
import { VectorsListComponent } from './vectors-list/vectors-list.component';
import { EditVectorComponent } from './edit-vector/edit-vector.component';
import { SelectVectorsForCommunitiesComponent } from './select-vectors-for-communities/select-vectors-for-communities.component';
import { EditCommunityBackgroundComponent } from './edit-community-background/edit-community-background.component';
import { AddMapVideosComponent } from './add-map-videos/add-map-videos.component';
import { ListCommunityVideosComponent } from './list-community-videos/list-community-videos.component';
import { GameAreaPreparatorComponent } from './game-area-preparator/game-area-preparator.component';
import { AddOpeningVideoComponent } from './add-opening-video/add-opening-video.component';
import { YoutubeEmbeddedComponent } from './youtube-embedded/youtube-embedded.component';

@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        EditCommunityComponent,
        CommunityDashboardComponent,
        CommunityChooserComponent,
        VectorsListComponent,
        EditVectorComponent,
        SelectVectorsForCommunitiesComponent,
        EditCommunityBackgroundComponent,
        AddMapVideosComponent,
        ListCommunityVideosComponent,
        GameAreaPreparatorComponent,
        AddOpeningVideoComponent,
        YoutubeEmbeddedComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    providers: [],
    bootstrap: [AppComponent],
    exports: [
        CommunityChooserComponent,
        YoutubeEmbeddedComponent
    ],
    entryComponents: [DashboardComponent]
})
export class AppModule {
}
