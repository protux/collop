import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GameAreaPreparatorComponent} from './game-area-preparator.component';

describe('GameAreaPreparatorComponent', () => {
    let component: GameAreaPreparatorComponent;
    let fixture: ComponentFixture<GameAreaPreparatorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [GameAreaPreparatorComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GameAreaPreparatorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
