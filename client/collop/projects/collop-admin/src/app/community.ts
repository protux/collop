import {Vector} from './vector';

export class Community {
    constructor(
        public name: string,
        public latitude: number,
        public longitude: number,
        public vectors: Vector[],
        public id?: number,
        public url?
    ) {
    }
}
