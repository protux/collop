import os

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Vector(models.Model):
    VECTOR_TYPE_DYNAMIC = 'dynamic'
    VECTOR_TYPE_STATIC = 'static'
    VECTOR_TYPES = (
        (VECTOR_TYPE_DYNAMIC, _('dynamic')),
        (VECTOR_TYPE_STATIC, _('static'))
    )

    name = models.CharField(max_length=100, verbose_name=_('Vector Name'))
    description = models.TextField(verbose_name=_('Description'), null=True, blank=True)
    video_id = models.CharField(max_length=30, verbose_name=_('YouTube-ID'))
    vector_type = models.CharField(max_length=10, choices=VECTOR_TYPES, verbose_name=_('Vector Type'))


class Community(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Community Name'))
    latitude = models.FloatField(verbose_name=_('Latitude'))
    longitude = models.FloatField(verbose_name=_('Longitude'))
    vectors = models.ManyToManyField(Vector)


class Map(models.Model):
    community = models.OneToOneField(
        Community,
        on_delete=models.CASCADE,
        related_name='maps'
    )
    image = models.ImageField(verbose_name=_('Map Image'))

    def __str__(self):
        return f'{self.community.name}: ${self.image.name}'


def generate_map_video_upload_dir(instance, filename):
    base_dir = os.path.join('map_videos', str(instance.community.id))
    return os.path.join(base_dir, filename)


class MapVideo(models.Model):
    community = models.ForeignKey(
        Community,
        on_delete=models.CASCADE,
        related_name='model_videos'
    )
    video_id = models.CharField(max_length=30, verbose_name=_('YouTube-ID'))
    thumbnail = models.ImageField(upload_to=generate_map_video_upload_dir, verbose_name=_('Thumbnail'))
    position_x = models.IntegerField(default=50)
    position_y = models.IntegerField(default=50)

    def __str__(self):
        return f'{self.community.name}: {self.video_id} @ {self.position_x}x{self.position_y}'


class Feedback(models.Model):
    map_video = models.ForeignKey(
        MapVideo,
        on_delete=models.CASCADE,
        related_name='feedbacks'
    )
    like = models.TextField()
    wish = models.TextField()
    give = models.TextField()


def generate_opening_video_upload_dir(instance, filename):
    base_dir = os.path.join('opening_videos', str(instance.community.id))
    return os.path.join(base_dir, filename)


class OpeningVideo(models.Model):
    community = models.ForeignKey(
        Community,
        on_delete=models.CASCADE,
        related_name='opening_videos'
    )
    video_id = models.CharField(max_length=30, verbose_name=_('YouTube-ID'))
    thumbnail = models.ImageField(upload_to=generate_opening_video_upload_dir, verbose_name=_('Thumbnail'))
