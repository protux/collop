from django.shortcuts import get_object_or_404

from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from collop.base import models as base_models
from . import serializers


class CommunityViewSet(viewsets.ModelViewSet):
    queryset = base_models.Community.objects.all()
    serializer_class = serializers.CommunitySerializer

    @action(methods=['post'], detail=True, url_path='add-vector')
    def add_vector(self, request, pk):
        vector_id = request.GET.get('vector', None)
        try:
            community = get_object_or_404(base_models.Community, pk=pk)
            vector = get_object_or_404(base_models.Vector, pk=vector_id)
            community.vectors.add(vector)
            community.save()
            return Response(status=status.HTTP_201_CREATED)
        except ValueError:
            return Response('vector must be the pk of a vector', status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['delete'], detail=True, url_path='remove-vector')
    def remove_vector(self, request, pk):
        vector_id = request.GET.get('vector', None)
        try:
            community = get_object_or_404(base_models.Community, pk=pk)
            vector = get_object_or_404(base_models.Vector, pk=vector_id)
            community.vectors.remove(vector)
            community.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except ValueError:
            return Response('vector must be the pk of a vector', status=status.HTTP_400_BAD_REQUEST)


class VectorViewSet(viewsets.ModelViewSet):
    queryset = base_models.Vector.objects.all()
    serializer_class = serializers.VectorSerializer


class CommunityFilteredViewSet(viewsets.ModelViewSet):

    def __init__(self, *args, **kwargs):
        self.community = None
        super().__init__(*args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()

        if self.request.method == 'GET':
            community = self.get_community()
            return queryset.filter(community=community)
        else:
            return queryset

    def get_community(self):
        community_id = self.request.GET.get('community', None)
        return get_object_or_404(base_models.Community, pk=community_id)


class MapViewSet(CommunityFilteredViewSet):
    queryset = base_models.Map.objects.all()
    serializer_class = serializers.MapSerializer


class MapVideoViewSet(CommunityFilteredViewSet):
    queryset = base_models.MapVideo.objects.all()
    serializer_class = serializers.MapVideoSerializer


class OpeningVideoViewSet(CommunityFilteredViewSet):
    queryset = base_models.OpeningVideo.objects.all()
    serializer_class = serializers.OpeningVideoSerializer


class FeedbackViewSet(viewsets.ModelViewSet):
    queryset = base_models.Feedback.objects.all()
    serializer_class = serializers.FeedbackSerializer

    def __init__(self, *args, **kwargs):
        self.map_video = None
        super().__init__(*args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()

        if self.request.method == 'GET':
            map_video = self.get_map_video()
            return queryset.filter(map_video=map_video)
        else:
            return queryset

    def get_map_video(self):
        map_video_id = self.request.GET.get('map_video', None)
        return get_object_or_404(base_models.MapVideo, pk=map_video_id)
