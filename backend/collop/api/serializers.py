from rest_framework import serializers

from collop.base import models as base_models


class VectorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = base_models.Vector
        fields = [
            'url',
            'id',
            'name',
            'video_id',
            'vector_type'
        ]


class CommunitySerializer(serializers.HyperlinkedModelSerializer):
    vectors = VectorSerializer(read_only=True, many=True)

    class Meta:
        model = base_models.Community
        fields = [
            'url',
            'id',
            'name',
            'latitude',
            'longitude',
            'vectors'
        ]


class MapSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = base_models.Map
        fields = [
            'community',
            'image',
        ]


class MapVideoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = base_models.MapVideo
        fields = [
            'url',
            'community',
            'video_id',
            'thumbnail',
            'position_x',
            'position_y',
        ]


class OpeningVideoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = base_models.OpeningVideo
        fields = [
            'url',
            'community',
            'video_id',
            'thumbnail',
        ]


class FeedbackSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = base_models.Feedback
        fields = [
            'url',
            'map_video',
            'like',
            'wish',
            'give'
        ]
