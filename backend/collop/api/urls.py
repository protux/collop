from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'communities', views.CommunityViewSet)
router.register(r'vectors', views.VectorViewSet)
router.register(r'maps', views.MapViewSet)
router.register(r'map-videos', views.MapVideoViewSet)
router.register(r'opening-videos', views.OpeningVideoViewSet)
router.register(r'feedbacks', views.FeedbackViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
